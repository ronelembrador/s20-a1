let testArray = [{
	id: 25,
	name: "Introduction to HTML",
	description: "Introduces HTML",
	price: "Php 1000",
	isActive: false,
},
{
	id: 50,
	name: "CSS Tips and Tricks",
	description: "Tips and tricks of CSS",
	price: "Php 2000",
	isActive: true,
}
];
const postCourse = (id, name, description, price, isActive) => {
	testArray.push({
		"id": id, 
		"name": name, 
		"description": description, 
		"price": price, 
		"isActive": isActive
	})

	alert(`You have created ${name}. The price is ${price}`);

}

postCourse(75, "JavaScript Basics", "Course tackling the basics of JavaScript", "Php 2500", true);

console.log(testArray);


let courseID = 75;
const found = testArray.find(element => element.id === courseID);
console.log(found);


const lastCourseDelete = () => testArray.pop();
lastCourseDelete();
console.log(testArray);